#include "led_behavior.h"

void behavior1(LED led, int frame) {
	int now = frame % 9;

	if (now == 0) {
		led.resetLED();
	} else {
		led.turnLED(now - 1, 1);
	}
}

void behavior2(LED led, int frame) {
	int now = frame % 9;

	if (now == 0) {
		led.resetLED();
	} else {
		led.turnLED(8 - now, 1);
	}
}

void behavior3(LED led, int frame) {
	int now = frame % 5;

	if (now == 0) {
		led.resetLED();
	} else {
		led.turnLED(now - 1, 1);
		led.turnLED(8 - now, 1);
	}
}
