#ifndef _LED_BEHAVIOR_H_
#define _LED_BEHAVIOR_H_

#include "../io/led.h"

void behavior1(LED, int);
void behavior2(LED, int);
void behavior3(LED, int);

#endif
