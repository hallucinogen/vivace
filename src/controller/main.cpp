#include "../io/led.h"
#include "../ports/port_mapper.h"
#include "../controller/led_behavior.h"

/**
 *
 * @brief this file contains the code entry point.
 * There should only be a single loop here, and a listener logic
 * the rest of the behavior should be tested somewhere else
 **/

int main(){
	int i;

	Port * port = new Port(PIN2);
	LED * led = new LED(port);

	i = 0;
  while(1) {
		behavior3(*led, i);
		++i;
  }

	delete led;
	return 0;
}
