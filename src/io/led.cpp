/** 
 * src/io/led.c
 * handles LED logic
 **/

#include "led.h"

void LED::setPort(Port * port) {
	this->port = port;
}

Port * LED::getPort() {
	return this->port;
}

LED::LED(Port * port) {
	this->setPort(port);
	this->resetLED();
}

LED::~LED() {
	delete this->port;
}

void LED::resetLED() {
	int i;

	for (i = 0; i < 8; ++i) {
		this->turnLED(i, 0);
	}
}

void LED::turnLED(int index, int isON) {
	if (isON) {
		this->getPort()->turnPinOn(index);
	} else {
		this->getPort()->turnPinOff(index);
	}
}
