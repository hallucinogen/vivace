#ifndef _LED_H_
#define _LED_H_

#include "../ports/port.h"

class LED {
private:
	Port * port;
public:
	/**
	 * make a LED object from given port.
	 * A LED object is a collection of 8 physical LED
	 * @param Port, the port to be regarded as a LED
	 **/
	LED(Port*);

	/**
	 * turn all LED off
	 **/
	void resetLED();

	/**
	 * turn a LED on/off
	 * @param index, LED index to be turned on/off
	 * @param isON, if isON, turn the LED on. Else, turn the LED off
	 **/
	void turnLED(int index, int isON);


	// getter / setter
	Port * getPort();
	void setPort(Port *);

	// destructor
	~LED();
};

#endif
