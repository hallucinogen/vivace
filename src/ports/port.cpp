#include "port.h"

Port::Port(LPC_GPIO_TypeDef * portRegister) {
	this->port = portRegister;

	// set all as output
	this->port->FIODIR = 0x000000FF;
}

void Port::turnPinOn(int index) {
	this->port->FIOSET = 1 << index;
}

void Port::turnPinOff(int index) {
	this->port->FIOCLR = 1 << index;
}

int Port::getPinsStatus() {
	return this->port->FIOPIN;
}
