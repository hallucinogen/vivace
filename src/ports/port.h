#ifndef _PORT_H_
#define _PORT_H_

#include <LPC17xx.H>

// TODO: accept generic port
class Port {
private:
	LPC_GPIO_TypeDef * port;

public:
	/**
	 * make a port object from given specific LPC port definition
	 * @param LPC_GPIO_TypeDef, LPC port definition
	 **/
	Port(LPC_GPIO_TypeDef *);

	/**
	 * turn a pin of given port on for a given index
	 * IS: pin of the port in given index is either on or off
	 * FS: pin of the port in given index is on, and doesn't affect other pin
	 *
	 * @param index, index of pin to be turned on
	 **/
	void turnPinOn(int);

	/**
	 * turn a pin of given port off for a given index
	 * IS: pin of the port in given index is either on or off
	 * FS: pin of the port in given index is off, and doesn't affect other pin
	 *
	 * @param index, index of pin to be turned off
	 **/
	void turnPinOff(int);

	/**
	 * get pins status of given port
	 **/
	int getPinsStatus();


};

#endif
